package xyz.duudl3.orion;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import org.bitcoinj.core.Address;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletChangeEventListener;
import org.spongycastle.util.encoders.Hex;
import xyz.duudl3.orion.tx.BroadcastHelper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class WalletController {
    public TextField address;
    public Label balance;
    public TextField amount;

    public void initialize()
    {
        address.setText(Main.walletAppKit.wallet().freshReceiveAddress().toString());
    }

    public void tipNode(ActionEvent actionEvent) {

    }

    public void onWalletSetup() {
        address.setText(Main.walletAppKit.wallet().freshReceiveAddress().toString());

        String currentBalance = Main.walletAppKit.wallet().getBalance(Wallet.BalanceType.ESTIMATED).toFriendlyString();
        balance.setText(currentBalance);

        Main.walletAppKit.wallet().addChangeEventListener(Platform::runLater, wallet -> {
            String currentBalance1 = Main.walletAppKit.wallet().getBalance(Wallet.BalanceType.ESTIMATED).toFriendlyString();
            balance.setText(currentBalance1);
        });
    }

    public void sendBitcoin(ActionEvent actionEvent) {
        if(!amount.getText().equals("") && amount.getText() != null) {
            new Thread() {
                @Override
                public void run() {
                    String address = "";
                    try {
                        address = Main.connHandler.getClient().getBitcoinAddress();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Coin coinAmt = Coin.parseCoin(amount.getText());

                    if(coinAmt.getValue() > 0.0)
                    {
                        Address destination = Address.fromString(Main.params, address);
                        SendRequest req = null;

                        if (coinAmt.getValue() >= Main.walletAppKit.wallet().getBalance().getValue())
                        {
                            req = SendRequest.emptyWallet(destination);
                        }
                        else
                        {
                            req = SendRequest.to(destination, coinAmt);
                        }

                        req.ensureMinRequiredFee = false;
                        //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
                        req.feePerKb = Coin.valueOf(5L * 1000L);
                        Transaction tx = null;
                        try {
                            tx = Main.walletAppKit.wallet().sendCoinsOffline(req);
                        } catch (InsufficientMoneyException e) {
                            e.printStackTrace();
                        }
                        byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
                        String txHex = null;
                        try {
                            txHex = new String(txHexBytes, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Created raw transaction: " + txHex);

                        System.out.println("Broadcasting raw transaction...");
                        BroadcastHelper helper = new BroadcastHelper(false, false);
                        helper.broadcast(txHex);

                        amount.setText("");
                    }
                }
            }.start();
        }
    }

    public void closeWindow(ActionEvent actionEvent) {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("main.fxml"));
        AnchorPane pane = null;
        try {
            pane = loader.<AnchorPane>load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Main.instance.controller = loader.getController();
        Main.instance.mainWindow.setTitle(Main.APP_NAME);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(getClass().getResource("styles.css").toString());
        Main.instance.mainWindow.setScene(scene);
        Main.instance.mainWindow.setResizable(false);
        Main.instance.mainWindow.setMaxWidth(800);
        Main.instance.mainWindow.setMaxHeight(451);
        Main.instance.mainWindow.show();
    }
}
