package xyz.duudl3.orion.crypto;

import xyz.duudl3.orion.Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESFileDecryption {
    public static void decryptFile(String fileName) throws Exception {
        //the hash.(origExt).des should be in the paramter above
        String encryptedExt = getFileExtension(new File("./client_dl/" + fileName));
        String fileBase = fileName.replace(encryptedExt, "");
        String originalExt = getFileExtension(new File("./client_dl/" + fileBase));
        String originalHash = fileBase.replace(originalExt, "");
        String password = Main.masterPass;

        // reading the salt
        // user should have secure mechanism to transfer the
        // salt, iv and password to the recipient
        FileInputStream saltFis = new FileInputStream("./client_dl/" + originalHash + "_salt" + originalExt + ".enc");
        byte[] salt = new byte[8];
        saltFis.read(salt);
        saltFis.close();

        // reading the iv
        FileInputStream ivFis = new FileInputStream("./client_dl/" + originalHash + "_iv" + originalExt + ".enc");
        byte[] iv = new byte[16];
        ivFis.read(iv);
        ivFis.close();

        SecretKeyFactory factory = SecretKeyFactory
                .getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, 65536,
                256);
        SecretKey tmp = factory.generateSecret(keySpec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

        // file decryption
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
        FileInputStream fis = new FileInputStream("./client_dl/" + fileName);
        FileOutputStream fos = new FileOutputStream("./client_dl/" + originalHash + originalExt);
        byte[] in = new byte[64];
        int read;
        while ((read = fis.read(in)) != -1) {
            byte[] output = cipher.update(in, 0, read);
            if (output != null)
                fos.write(output);
        }

        byte[] output = cipher.doFinal();
        if (output != null)
            fos.write(output);
        fis.close();
        fos.flush();
        fos.close();
        File fileDes = new File("./client_dl/" + originalHash + originalExt + ".des");
        File fileSalt = new File("./client_dl/" + originalHash + "_salt" + originalExt + ".enc");
        File fileIv = new File("./client_dl/" + originalHash + "_iv" + originalExt + ".enc");
        fileDes.delete();
        fileSalt.delete();
        fileIv.delete();
        System.out.println("[CLIENT] " + originalHash + originalExt + ".des decrypted.");
    }

    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }
}