/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.orion;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import xyz.duudl3.orion.utils.UIModel;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

/**
 * Gets created auto-magically by FXMLLoader via reflection. The widget fields are set to the GUI controls they're named
 * after. This class handles all the updates and event handling for the main UI.
 */
public class MainController {
    public ListView fileList;
    public static UIModel model = new UIModel();
    public static int fileSelected;
    public TextField filePath;
    public TextField decryptionKey;
    public TextField directDownloadFileName;
    public TextField uploadedFileHash;
    public Button btnUpload;
    public ProgressBar progressBar;
    public Button btnDownload;
    public Label maxSizeWarning;
    public Button btnWallet;
    private String filePathAbsolute;

    // Called by FXMLLoader.
    public void initialize() {
        Bindings.bindContent(fileList.getItems(), model.getFiles());
    }

    public void onServerConnected()
    {
        Bindings.bindContent(fileList.getItems(), model.getFiles());
    }

    public void downloadFile(MouseEvent mouseEvent) {
        if(fileList.getSelectionModel().getSelectedItem() != null && !decryptionKey.getText().equals("") && decryptionKey.getText() != null) {
            new Thread() {
                @Override
                public void run() {
                    Main.masterPass = decryptionKey.getText();
                    btnUpload.setDisable(true);
                    btnDownload.setDisable(true);
                    progressBar.setVisible(true);
                    fileSelected = fileList.getSelectionModel().getSelectedIndex();
                    String file = Main.connHandler.getClient().files.get(fileSelected);
                    try {
                        Main.connHandler.getClient().getFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    btnUpload.setDisable(false);
                    btnDownload.setDisable(false);
                    progressBar.setVisible(false);
                }
            }.start();
        }
    }

    public void openWallet(ActionEvent actionEvent)
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("wallet.fxml"));
        AnchorPane pane = null;
        try {
            pane = loader.<AnchorPane>load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Main.instance.walletController = loader.getController();
        Main.instance.mainWindow.setTitle(Main.APP_NAME);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add(getClass().getResource("styles.css").toString());
        Main.instance.mainWindow.setScene(scene);
        Main.instance.mainWindow.setResizable(false);
        Main.instance.mainWindow.setMaxWidth(800);
        Main.instance.mainWindow.setMaxHeight(451);
        Main.instance.mainWindow.show();

        Platform.runLater(Main.instance.walletController::onWalletSetup);
    }

    public void openFileBrowser(ActionEvent actionEvent) {
        final FileChooser fileChooser = new FileChooser();

        File file = fileChooser.showOpenDialog(Main.instance.mainWindow);
        if (file != null) {
            long fileSize = file.length();

            if(fileSize < 2147483647) {
                maxSizeWarning.setVisible(false);
                filePath.setText(file.getPath());
                btnUpload.setDisable(false);
                filePathAbsolute = filePath.getText().replace("\\", "\\\\");
            }
            else
            {
                maxSizeWarning.setVisible(true);
                btnUpload.setDisable(true);
            }
        }
    }

    public void upload(ActionEvent actionEvent) {
        if(filePathAbsolute != null && !filePathAbsolute.equals("") && decryptionKey.getText() != null && !decryptionKey.getText().equals("")) {
            new Thread()
            {
                @Override
                public void run()
                {
                    Main.masterPass = decryptionKey.getText();
                    File fileToSend = new File(filePathAbsolute);
                    btnUpload.setDisable(true);
                    btnDownload.setDisable(true);
                    progressBar.setVisible(true);
                    String hash = getFileHash(fileToSend).toLowerCase();
                    String extension = getFileExtension(fileToSend);
                    try {
                        Main.connHandler.getClient().sendFile(filePathAbsolute);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    uploadedFileHash.setText(hash + extension + ".des");
                    btnDownload.setDisable(false);
                    btnUpload.setDisable(false);
                    progressBar.setVisible(false);
                }
            }.start();
        }
    }

    public void directDownload(ActionEvent actionEvent) {
        if(!directDownloadFileName.getText().equals("") && directDownloadFileName.getText() != null && !decryptionKey.getText().equals("") && decryptionKey.getText() != null) {
            new Thread() {
                @Override
                public void run() {
                    Main.masterPass = decryptionKey.getText();
                    btnUpload.setDisable(true);
                    btnDownload.setDisable(true);
                    progressBar.setVisible(true);
                    String file = directDownloadFileName.getText();
                    try {
                        Main.connHandler.getClient().getFile(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    btnDownload.setDisable(false);
                    btnUpload.setDisable(false);
                    progressBar.setVisible(false);
                }
            }.start();
        }
    }

    public String getFileHash(File input) {
        try (InputStream in = new FileInputStream(input)) {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] block = new byte[(int)input.length()];
            int length;
            while ((length = in.read(block)) > 0) {
                digest.update(block, 0, length);
            }

            return DatatypeConverter.printHexBinary(digest.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }
}
