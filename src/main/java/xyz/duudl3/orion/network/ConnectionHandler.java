package xyz.duudl3.orion.network;

import java.io.IOException;

import xyz.duudl3.orion.client.ClientConnection;

public class ConnectionHandler {
	private ClientConnection clientConn;

	public ConnectionHandler()
	{
	}

	public void initialize(String ip) throws IOException
	{
		clientConn = new ClientConnection(ip);
	}

	public ClientConnection getClient()
	{
		return clientConn;
	}
}
