/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.orion.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import xyz.duudl3.orion.Main;

/**
 * A class that exposes relevant myntcurrency stuff as JavaFX bindable properties.
 */
public class UIModel
{
    public static ObservableList<String> files = FXCollections.observableArrayList();

    public UIModel() {
        update();
    }

    public void update()
    {
        files.clear();
        files.setAll(Main.connHandler.getClient().files);
    }

    public ObservableList<String> getFiles() {
        return files;
    }
}
