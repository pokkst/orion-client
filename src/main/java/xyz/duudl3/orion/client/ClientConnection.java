package xyz.duudl3.orion.client;

import xyz.duudl3.orion.Main;
import xyz.duudl3.orion.MainController;
import xyz.duudl3.orion.crypto.AESFileDecryption;
import xyz.duudl3.orion.crypto.AESFileEncryption;
import xyz.duudl3.orion.network.NetworkConstants;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

public class ClientConnection {
    private Socket clientSocket;
    public ArrayList<String> files = new ArrayList<String>();
    private String server;
    public ClientConnection(String server) {
        this.server = server;

        try {
            System.out.println("[CLIENT] Initiating our client. Connecting to " + server + ":" + NetworkConstants.PORT);
            requestFileList();

            Thread.sleep(200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Socket getClientConnection()
    {
        return clientSocket;
    }

    public String getBitcoinAddress() throws IOException {
        clientSocket = new Socket(server, NetworkConstants.PORT);
        DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());

        /**
         * Notify the nodes what file we want by giving them the file hash.
         */
        dos1.writeInt(3);

        BufferedInputStream bis1 = new BufferedInputStream(getClientConnection().getInputStream());
        DataInputStream dis1 = new DataInputStream(bis1);
        //Getting file extension
        String address = dis1.readUTF();

        dis1.close();
        bis1.close();
        dos1.close();

        return address;
    }

    public void sendFile(String file) throws Exception {
        //Encrypts file and gets hash of the original file.
        String extension = getFileExtension(new File(file));
        String hash = AESFileEncryption.encryptFile(file);
        sendEncryptedFile(hash + extension, "des");
        sendEncryptedFile(hash + "_salt" + extension, "enc");
        sendEncryptedFile(hash + "_iv" + extension, "enc");
        File fileDes = new File("./" + hash + extension + ".des");
        File fileSalt = new File("./" + hash + "_salt" + extension + ".enc");
        File fileIv = new File("./" + hash + "_iv" + extension + ".enc");
        fileDes.delete();
        fileSalt.delete();
        fileIv.delete();
        Main.connHandler.getClient().requestFileList();
        MainController.model.update();
    }

    public void sendEncryptedFile(String hash, String type) throws IOException {
        clientSocket = new Socket(server, NetworkConstants.PORT);

        DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());
        /**
         * Notify the nodes we're sending data to of the file type we're about to send.
         */
        File encryptedFile = new File("./" + hash + "." + type);

        dos1.writeInt(0);
        dos1.writeUTF(getFileExtension(encryptedFile));
        dos1.writeUTF(hash);
        /**
         * Begin sending the data to the nodes.
         */
        FileInputStream fis = new FileInputStream(encryptedFile.getPath());
        dos1.writeInt((int)encryptedFile.length());
        byte[] buffer = new byte[(int) encryptedFile.length()];

        while (fis.read(buffer) > 0) {
            dos1.write(buffer);
        }

        fis.close();
        dos1.close();
        clientSocket.close();
    }

    public void getFile(String fileHash) throws IOException {
        clientSocket = new Socket(server, NetworkConstants.PORT);
        DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());

        /**
         * Notify the nodes what file we want by giving them the file hash.
         */
        dos1.writeInt(1);
        dos1.writeUTF(fileHash);

        /**
         * Here we receive a response from the server with the file data.
         */
        BufferedInputStream bis1 = new BufferedInputStream(getClientConnection().getInputStream());
        DataInputStream dis1 = new DataInputStream(bis1);
        //Getting file extension
        int files = dis1.readInt();

        for(int x = 0; x < files; x++)
        {
            long fileSize = dis1.readLong();
            String fileToDLName = dis1.readUTF();
            File fileToDownload = new File("./client_dl/" + fileToDLName);
            String cachedFileHash = fileToDLName;
            cachedFileHash = cachedFileHash.replace(getFileExtension(fileToDownload), "");
            String fileExtension = dis1.readUTF();

            System.out.println("[CLIENT] Downloading encrypted file: " + cachedFileHash + fileExtension + " with file size of " + fileSize);

            long time = System.currentTimeMillis();
            String fileName = "./client_dl/" + getStringHash("" + time) + fileExtension;

            /**
             * Prepare to read the file the server is sending.
             */
            FileOutputStream fos = new FileOutputStream("./client_dl/" + cachedFileHash + fileExtension);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            /**
             * Read the data.
             */
            for(int j = 0; j < fileSize; j++) bos.write(bis1.read());

            //Close the file
            bos.close();
            fos.close();
        }

        dis1.close();
        bis1.close();
        dos1.close();
        clientSocket.close();
        System.out.println("[CLIENT] Downloaded encrypted version of requested file.");
        try {
            AESFileDecryption.decryptFile(fileHash);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestFileList() throws IOException {
        clientSocket = new Socket(server, NetworkConstants.PORT);
        files.clear();
        try {
            DataOutputStream dos1 = new DataOutputStream(getClientConnection().getOutputStream());

            /**
             * Notify the nodes what file we want by giving them the file hash.
             */
            try {
                dos1.writeInt(2);
            } catch (IOException e) {
                e.printStackTrace();
            }

            /**
             * Here we receive a response from the server with the file data.
             */
            BufferedInputStream bis1 = new BufferedInputStream(getClientConnection().getInputStream());
            DataInputStream dis1 = new DataInputStream(bis1);
            int fileCount = dis1.readInt();

            for (int i = 0; i < fileCount; i++) {
                long fileSize = dis1.readLong();
                String fileName = dis1.readUTF();
                files.add(fileName);
            }

            dis1.close();
            bis1.close();
            dos1.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

        clientSocket.close();
    }

    public String getStringHash(String value)
    {
        try{
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(value.getBytes());
            return bytesToHex(md.digest());
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private String bytesToHex(byte[] bytes)
    {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }
}
