/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.orion;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import xyz.duudl3.orion.network.ConnectionHandler;
import xyz.duudl3.orion.utils.UIModel;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

/**
 * Gets created auto-magically by FXMLLoader via reflection. The widget fields are set to the GUI controls they're named
 * after. This class handles all the updates and event handling for the main UI.
 */
public class ConnectController {
    public TextField ip;

    // Called by FXMLLoader.
    public void initialize() {

    }


    public void connectToNode(ActionEvent actionEvent) {
        if(ip.getText() != null && !ip.getText().equals("")) {
            Main.connHandler = new ConnectionHandler();
            try {
                Main.connHandler.initialize(ip.getText());
            } catch (IOException e) {
                e.printStackTrace();
            }

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("main.fxml"));
            AnchorPane pane = null;
            try {
                pane = loader.<AnchorPane>load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Main.instance.controller = loader.getController();
            Main.instance.mainWindow.setTitle(Main.APP_NAME);
            Scene scene = new Scene(pane);
            scene.getStylesheets().add(getClass().getResource("styles.css").toString());
            Main.instance.mainWindow.setScene(scene);
            Main.instance.mainWindow.setResizable(false);
            Main.instance.mainWindow.setMaxWidth(800);
            Main.instance.mainWindow.setMaxHeight(451);
            Main.instance.mainWindow.show();

            if (Main.connHandler.getClient().getClientConnection().isConnected())
                Main.instance.controller.onServerConnected();
        }
    }
}
