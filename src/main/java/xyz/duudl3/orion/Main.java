/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package xyz.duudl3.orion;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Wallet;
import xyz.duudl3.orion.network.ConnectionHandler;
import xyz.duudl3.orion.utils.GuiUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import static xyz.duudl3.orion.utils.GuiUtils.*;

public class Main extends Application {
    public static NetworkParameters params = null;
    public static final String APP_NAME = "Orion";
    private static String WALLET_FILE_NAME = "orion_wallet";
    public static WalletAppKit walletAppKit;
    public static Main instance;

    private StackPane uiStack;
    private Pane mainUI;
    public MainController controller;
    public ConnectController connectController;
    public WalletController walletController;

    public Stage mainWindow;
    public static ConnectionHandler connHandler;
    public static String masterPass = "00000000000000";

    public static void main(String[] args) throws IOException {
        //Variable is never actually used, we just mkdir each time just in case it doesn't exist.
        boolean client_dir = new File("./client_dl/").mkdir();
        params = MainNetParams.get();
        launch(args);
    }

    @Override
    public void start(Stage mainWindow) throws Exception {
        try {
            loadWindow(mainWindow);
        } catch (Throwable e) {
            GuiUtils.crashAlert(e);
            throw e;
        }
    }

    private void loadWindow(Stage mainWindow) throws IOException {
        this.mainWindow = mainWindow;
        instance = this;
        FXMLLoader loader2 = new FXMLLoader();
        loader2.setLocation(getClass().getResource("connect.fxml"));
        AnchorPane pane2 = loader2.<AnchorPane>load();
        connectController = loader2.getController();
        Scene scene2 = new Scene(pane2);
        scene2.getStylesheets().add(getClass().getResource("styles.css").toString());
        mainWindow.setScene(scene2);
        mainWindow.setResizable(false);
        mainWindow.setMaxWidth(800);
        mainWindow.setMaxHeight(451);
        mainWindow.getIcons().add(new Image("https://duudl3.xyz/img/orion_client.png"));

        Threading.USER_THREAD = Platform::runLater;

        setupWalletKit(null);

        if (walletAppKit.isChainFileLocked()) {
            informationalAlert("Orion Notification", "Orion is already running and cannot be started twice.");
            Platform.exit();
            return;
        }

        mainWindow.show();
    }

    public void setupWalletKit(DeterministicSeed seed) {
        // If seed is non-null it means we are restoring from backup.
        walletAppKit = new WalletAppKit(params, new File("."), WALLET_FILE_NAME)
        {
            @Override
            protected void onSetupCompleted() {
                walletAppKit.wallet().allowSpendingUnconfirmedTransactions();
            }
        };

        walletAppKit.setDownloadListener(new DownloadProgressTracker() {
            @Override
            protected void progress(double pct, int blocksSoFar, Date date) {
                super.progress(pct, blocksSoFar, date);
                int percentage = (int) pct;
            }

            @Override
            protected void doneDownload() {
                super.doneDownload();
                //watchingWallet = Wallet.fromWatchingKey(parameters, DeterministicKey.deserializeB58(null, serialized, parameters));
            }
        });

        walletAppKit.setBlockingStartup(false).setUserAgent(APP_NAME, "1.0");

        if (seed != null)
            walletAppKit.restoreWalletFromSeed(seed);

        walletAppKit.startAsync();
    }

    private Node stopClickPane = new Pane();

    public class OverlayUI<T>
    {
        public Node ui;
        public T controller;

        public OverlayUI(Node ui, T controller)
        {
            this.ui = ui;
            this.controller = controller;
        }

        public void show()
        {
            checkGuiThread();
            if (currentOverlay == null)
            {
                uiStack.getChildren().add(stopClickPane);
                uiStack.getChildren().add(ui);
                blurOut(mainUI);
                fadeIn(ui);
                zoomIn(ui);
            }
            else
            {
                explodeOut(currentOverlay.ui);
                fadeOutAndRemove(uiStack, currentOverlay.ui);
                uiStack.getChildren().add(ui);
                ui.setOpacity(0.0);
                fadeIn(ui, 100);
                zoomIn(ui, 100);
            }
            currentOverlay = this;
        }

        public void done() {
            checkGuiThread();
            if (ui == null) return;  // In the middle of being dismissed and got an extra click.
            explodeOut(ui);
            fadeOutAndRemove(uiStack, ui, stopClickPane);
            blurIn(mainUI);
            this.ui = null;
            this.controller = null;
            currentOverlay = null;
        }
    }

    private OverlayUI currentOverlay;

    //loads given .fxml file and displays it onto the Scene.
    public <T> OverlayUI<T> overlayUI(String name)
    {
        try
        {
            checkGuiThread();
            URL location = GuiUtils.getResource(name);
            FXMLLoader loader = new FXMLLoader(location);
            Pane ui = loader.load();
            T controller = loader.getController();
            OverlayUI<T> pair = new OverlayUI<T>(ui, controller);

            try
            {
                if (controller != null)
                    controller.getClass().getField("overlayUI").set(controller, pair);
            }
            catch (IllegalAccessException | NoSuchFieldException ignored)
            {
                //my program is fucking flawless so i wont need this shit tbh
                ignored.printStackTrace();
            }

            pair.show();
            return pair;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void stop() throws Exception {
        walletAppKit.stopAsync();
        walletAppKit.awaitTerminated();
        System.exit(0);
    }
}
