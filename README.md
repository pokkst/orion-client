# Orion Client

The client implementation of the Orion end-to-end encrypted cloud storage solution.

(Transferred from my old GitLab account)

# Compiling

Import this project into IntelliJ, then run clean and package through Maven.

After doing that, in /target/ an Orion.jar file will appear.

Go into /lib/ and copy the contents of the BitcoinJ .jar file (everything BUT META-INF) into Orion.jar

It should be good to go. :)
